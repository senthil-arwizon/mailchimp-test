<?php

/**
 * Listrak Remarketing Magento Extension Ver. 2.0.0
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2015 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

namespace Listrak\Remarketing\Controller\Cart;

class Reload extends \Magento\Framework\App\Action\Action
{
    private $cartHelper = null;
    
    private $checkoutSession = null;
    
    private $quoteFactory = null;
    
    private $logger = null;
    
    private $sourceQuote = null;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    public function __construct(
        \Listrak\Remarketing\Helper\Cart $cartHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->cartHelper = $cartHelper;
        $this->checkoutSession = $checkoutSession;
        $this->quoteFactory = $quoteFactory;
        $this->logger = $logger;
        parent::__construct($context);
    }
    
    public function execute()
    {
        try {
            if ($this->loadSourceQuote()) {
                $this->copyItems();
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        $query = $this->getRequest()->getParams();
        $this->logger->info('in execute redirectToCart : query :: '. $query);
        return $this->redirectToCart();
    }
    
    private function loadSourceQuote()
    {
        $basketId = $this->getRequest()->getParam('ltksid');
        $quoteId = $this->cartHelper->getQuoteIdFromBasketId($basketId);
        if ($quoteId === false
            || $quoteId == $this->checkoutSession->getQuoteId()
            || $quoteId == $this->checkoutSession->getListrakMergedQuoteId()
        ) {
            return false;
        }
        
        $quote = $this->quoteFactory->create()->loadActive($quoteId);
        if (!$quote->hasEntityId()) {
            return false;
        }
        
        $this->sourceQuote = $quote;
        return true;
    }
    
    private function copyItems()
    {
        $destQuote = $this->checkoutSession->getQuote();
        if (!$destQuote) {
            $destQuote = $this->quoteFactory->create();
        }
        
        $destQuote->merge($this->sourceQuote)
            ->collectTotals()
            ->save();
        
        if ($this->checkoutSession->getQuoteId() != $destQuote->getEntityId()) {
            $this->checkoutSession->setQuoteId($destQuote->getEntityId());
        }

        $this->checkoutSession->setListrakMergedQuoteId(
            $this->sourceQuote->getEntityId()
        );
    }
    
    private function redirectToCart()
    {
        $query = $this->getRequest()->getParams();
        $this->logger->info('redirectToCart : query :: '. $query);
        unset($query["redirectUrl"]);
        unset($query["ltksid"]);

        $url = $this->getRequest()->getParam('redirectUrl');
        if (!$url) {
            $url = 'checkout/cart/';
        }
        $this->logger->info('redirectToCart : url :: '. $url);
        $this->logger->info('redirectToCart : After Processing the query :: '. $query);

        return $this->_redirect(
            $url,
            [
                '_query' => $query,
                '_secure' => $this->getRequest()->isSecure()
            ]
        );
    }
}
